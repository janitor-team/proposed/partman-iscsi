# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Turkish messages for debian-installer.
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Recai Oktaş <roktas@omu.edu.tr>, 2004, 2005, 2008.
# Osman Yüksel <yuxel@sonsuzdongu.com>, 2004.
# Özgür Murat Homurlu <ozgurmurat@gmx.net>, 2004.
# Halil Demirezen <halild@bilmuh.ege.edu.tr>, 2004.
# Murat Demirten <murat@debian.org>, 2004.
# Mert Dirik <mertdirik@gmail.com>, 2008-2012, 2014.
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@computer.org>, 2001.
# (translations from drakfw)
# Fatih Demir <kabalak@gmx.net>, 2000.
# Free Software Foundation, Inc., 2000,2004
# Kemal Yilmaz <kyilmaz@uekae.tubitak.gov.tr>, 2001.
# Mert Dirik <mertdirik@gmail.com>, 2008, 2014.
# Nilgün Belma Bugüner <nilgun@fide.org>, 2001.
# Recai Oktaş <roktas@omu.edu.tr>, 2004.
# Tobias Quathamer <toddy@debian.org>, 2007.
# Translations taken from ICU SVN on 2007-09-09
# Ömer Fadıl USTA <omer_fad@hotmail.com>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: partman-iscsi@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:50+0100\n"
"PO-Revision-Date: 2015-06-30 01:49+0200\n"
"Last-Translator: Mert Dirik <mertdirik@gmail.com>\n"
"Language-Team: Debian L10N Turkish\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: text
#. Description
#. :sl3:
#: ../partman-iscsi.templates:1001
msgid "Configure iSCSI volumes"
msgstr "iSCSI ciltlerini yapılandır"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-iscsi.templates:2001
msgid "Log into iSCSI targets"
msgstr "iSCSI hedeflerine oturum aç"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-iscsi.templates:2001
msgid "Finish"
msgstr "Bitir"

#. Type: select
#. Description
#. :sl3:
#: ../partman-iscsi.templates:2002
msgid "iSCSI configuration actions"
msgstr "iSCSI yapılandırma eylemleri"

#. Type: select
#. Description
#. :sl3:
#: ../partman-iscsi.templates:2002
msgid "This menu allows you to configure iSCSI volumes."
msgstr "Bu menü iSCSI ciltlerini yapılandırmaya yarar."

#. Type: string
#. Description
#. :sl3:
#: ../partman-iscsi.templates:3001
msgid "iSCSI target portal address:"
msgstr "iSCSI hedef portal adresi:"

#. Type: string
#. Description
#. :sl3:
#: ../partman-iscsi.templates:3001
msgid ""
"Enter an IP address to scan for iSCSI targets. To use a port other than the "
"default of 3260, use \"IP:port\" notation, for example \"1.2.3.4:3261\"."
msgstr ""
"iSCSI hedefleri için taranacak olan IP adreslerini girin. öntanımlı olan "
"3260 numaralı porttan farklı bir port kullanmak isterseniz \"IP:port\" "
"gösterimini kullanın. Örnek bir kullanım \"1.2.3.4:3261\" şeklinde olabilir."

#. Type: string
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:4001
msgid "iSCSI initiator username for ${PORTAL}:"
msgstr "${PORTAL} konumunun iSCSI başlatıcı kullanıcı adı:"

#. Type: string
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:4001
msgid ""
"Some iSCSI targets require the initiator (client) to authenticate using a "
"username and password. If that is the case for this target, enter the "
"username here. Otherwise, leave this blank."
msgstr ""
"Bazı iSCSI hedefleri başlatıcının (istemcinin) kimlik doğrulama amacıyla bir "
"kullanıcı adı ve parola sağlamasını gerektirir. Sizin kullanmak istediğiniz "
"hedef için de böyle bir durum geçerliyse buraya gereken kullanıcı adını "
"girin. Aksi takdirde bu alanı boş bırakın."

#. Type: password
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:5001
msgid "iSCSI initiator password for ${PORTAL}:"
msgstr "${PORTAL} konumu için iSCSI başlatıcı parolası:"

#. Type: password
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:5001
msgid ""
"Enter the initiator password needed to authenticate to this iSCSI target."
msgstr ""
"Bu iSCSI hedefinde kimlik doğrulamak için kullanılacak olan parolayı girin."

#. Type: string
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:6001
msgid "iSCSI target username for ${PORTAL}:"
msgstr "${PORTAL} konumu için iSCSI hedef kullanıcı adı:"

#. Type: string
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:6001
msgid ""
"In some environments, the iSCSI target needs to authenticate to the "
"initiator as well as the other way round. If that is the case here, enter "
"the incoming username which the target is expected to supply. Otherwise, "
"leave this blank."
msgstr ""
"Bazı ortamlarda, aynı başlatıcının iSCSI hedefine kimlik doğrulaması "
"gerektiği gibi, iSCSI hedefinin de başlatıcıya kimliğini doğrulaması "
"gerekir. Böyle bir durumla karşı karşıyaysanız, hedefin başlatıcıya "
"sağlaması gereken kullanıcı adını buraya yazın. Aksi takdirde burayı boş "
"bırakabilirsiniz."

#. Type: password
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:7001
msgid "iSCSI target password for ${PORTAL}:"
msgstr "${PORTAL} konumu için iSCSI hedef parolası:"

#. Type: password
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:7001
msgid ""
"Enter the incoming password which the iSCSI target is expected to supply."
msgstr "iSCSI hedefinin sağlaması gereken gelen parolayı buraya girin."

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:8001
msgid "Empty password"
msgstr "Boş parola"

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:8001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"Boş bir parola girdiniz. Buna izin verilmiyor. Lütfen boş olmayan bir parola "
"girin."

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:9001
msgid "No iSCSI targets discovered"
msgstr "Hiç iSCSI hedefi keşfedilemedi"

#. Type: error
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:9001
msgid "No iSCSI targets were discovered on ${PORTAL}."
msgstr "${PORTAL} konumunda hiç iSCSI hedefi keşfedilemedi."

#. Type: multiselect
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:11001
msgid "iSCSI targets on ${PORTAL}:"
msgstr "${PORTAL} konumundaki iSCSI hedefleri:"

#. Type: multiselect
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:11001
msgid "Select the iSCSI targets you wish to use."
msgstr "Lütfen kullanmak istediğiniz iSCSI hedeflerini seçin."

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:12001
msgid "iSCSI login failed"
msgstr "iSCSI oturum açma başarısız"

#. Type: error
#. Description
#. :sl3:
#. Translators : do NOT translate the variable names (PORTAL and TARGET)
#: ../partman-iscsi.templates:12001
msgid "Logging into the iSCSI target ${TARGET} on ${PORTAL} failed."
msgstr "${PORTAL} konumundaki ${TARGET} iSCSI hedefine oturum açılamadı."

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:12001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr ""
"Ayrıntılı bilgi için /var/log/syslog dosyasına veya 4 numaralı konsola bakın."
