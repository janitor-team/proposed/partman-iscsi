# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
#
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
#
#
# Danishka Navin <danishka@gmail.com>, 2009, 2011, 2012.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: partman-iscsi@packages.debian.org\n"
"POT-Creation-Date: 2019-11-02 21:50+0100\n"
"PO-Revision-Date: 2012-02-28 10:16+0530\n"
"Last-Translator: Danishka Navin <danishka@gmail.com>\n"
"Language-Team: Sinhala <info@hanthana.org>\n"
"Language: si\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: text
#. Description
#. :sl3:
#: ../partman-iscsi.templates:1001
#, fuzzy
msgid "Configure iSCSI volumes"
msgstr "ගුප්තකේතිත වෙළුම් සකසන්න"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-iscsi.templates:2001
msgid "Log into iSCSI targets"
msgstr ""

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages)
#. :sl3:
#: ../partman-iscsi.templates:2001
msgid "Finish"
msgstr "අවසන්"

#. Type: select
#. Description
#. :sl3:
#: ../partman-iscsi.templates:2002
#, fuzzy
msgid "iSCSI configuration actions"
msgstr "ZFS සැකසුම් ක්‍රියාව:"

#. Type: select
#. Description
#. :sl3:
#: ../partman-iscsi.templates:2002
#, fuzzy
msgid "This menu allows you to configure iSCSI volumes."
msgstr "මෙම මෙනුව ඔබට ගුප්තකේතිත වෙළුම් සැකසීමට ඉඩදෙයි."

#. Type: string
#. Description
#. :sl3:
#: ../partman-iscsi.templates:3001
msgid "iSCSI target portal address:"
msgstr ""

#. Type: string
#. Description
#. :sl3:
#: ../partman-iscsi.templates:3001
msgid ""
"Enter an IP address to scan for iSCSI targets. To use a port other than the "
"default of 3260, use \"IP:port\" notation, for example \"1.2.3.4:3261\"."
msgstr ""

#. Type: string
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:4001
msgid "iSCSI initiator username for ${PORTAL}:"
msgstr ""

#. Type: string
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:4001
msgid ""
"Some iSCSI targets require the initiator (client) to authenticate using a "
"username and password. If that is the case for this target, enter the "
"username here. Otherwise, leave this blank."
msgstr ""

#. Type: password
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:5001
msgid "iSCSI initiator password for ${PORTAL}:"
msgstr ""

#. Type: password
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:5001
msgid ""
"Enter the initiator password needed to authenticate to this iSCSI target."
msgstr ""

#. Type: string
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:6001
msgid "iSCSI target username for ${PORTAL}:"
msgstr ""

#. Type: string
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:6001
msgid ""
"In some environments, the iSCSI target needs to authenticate to the "
"initiator as well as the other way round. If that is the case here, enter "
"the incoming username which the target is expected to supply. Otherwise, "
"leave this blank."
msgstr ""

#. Type: password
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:7001
msgid "iSCSI target password for ${PORTAL}:"
msgstr ""

#. Type: password
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:7001
msgid ""
"Enter the incoming password which the iSCSI target is expected to supply."
msgstr ""

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:8001
msgid "Empty password"
msgstr "හිස් මුරපදයක්"

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:8001
msgid ""
"You entered an empty password, which is not allowed. Please choose a non-"
"empty password."
msgstr ""
"ඔබ විසින් හිස් මුරපදයක් ඇතුළු කර ඇත, එයට ඉඩදිය නොහැකි බැවින් එසේ නොවන මුරපදයක් ඇතුළත් කරන්න."

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:9001
msgid "No iSCSI targets discovered"
msgstr ""

#. Type: error
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:9001
msgid "No iSCSI targets were discovered on ${PORTAL}."
msgstr ""

#. Type: multiselect
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:11001
msgid "iSCSI targets on ${PORTAL}:"
msgstr ""

#. Type: multiselect
#. Description
#. :sl3:
#. Translators : do NOT translate the variable name (PORTAL)
#: ../partman-iscsi.templates:11001
#, fuzzy
msgid "Select the iSCSI targets you wish to use."
msgstr "කරුණාකර මකාදැමීමට අවශ්‍ය ZFS එකතුව තෝරන්න."

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:12001
#, fuzzy
msgid "iSCSI login failed"
msgstr "SILO ස්ථාපනය අසාර්ථකයි"

#. Type: error
#. Description
#. :sl3:
#. Translators : do NOT translate the variable names (PORTAL and TARGET)
#: ../partman-iscsi.templates:12001
msgid "Logging into the iSCSI target ${TARGET} on ${PORTAL} failed."
msgstr ""

#. Type: error
#. Description
#. :sl3:
#: ../partman-iscsi.templates:12001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "තොරතුරු සඳහා /var/log/syslog හෝ 4 වැනි අතත්‍ය කොන්සෝලය පිරික්සන්න."
